package com.bbyb.operating.examination.model.po;

import java.time.LocalDateTime;

public class ExaminationPaper {
    private Integer id;

    private String paperName;

    private String paperCode;

    private Integer subjectId;

    private LocalDateTime examinationStartTime;

    private LocalDateTime examinationEndTime;

    private Integer planId;

    private String createBy;

    private LocalDateTime createTime;

    private String updateBy;

    private LocalDateTime updateTime;

    private Integer invalid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public String getPaperCode() {
        return paperCode;
    }

    public void setPaperCode(String paperCode) {
        this.paperCode = paperCode;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public LocalDateTime getExaminationStartTime() {
        return examinationStartTime;
    }

    public void setExaminationStartTime(LocalDateTime examinationStartTime) {
        this.examinationStartTime = examinationStartTime;
    }

    public LocalDateTime getExaminationEndTime() {
        return examinationEndTime;
    }

    public void setExaminationEndTime(LocalDateTime examinationEndTime) {
        this.examinationEndTime = examinationEndTime;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getInvalid() {
        return invalid;
    }

    public void setInvalid(Integer invalid) {
        this.invalid = invalid;
    }
}