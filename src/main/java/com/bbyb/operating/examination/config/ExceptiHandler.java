package com.bbyb.operating.examination.config;

import com.bbyb.operating.examination.model.vo.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author 姚某
 * data 2023/8/10 17:00
 * fileName ExceptiHandler
 */
@RestControllerAdvice
@Slf4j
public class ExceptiHandler {

    @ExceptionHandler(value = Exception.class)
    public CommonResult ExceptionHandler(Exception exception) {
        String className = exception.getStackTrace()[0].getClassName();
        String methodName = exception.getStackTrace()[0].getMethodName();
        log.info("异常类名为【{}】 异常方法名为【{}】",className,methodName);

        return new CommonResult() {{
           setCode(500);
           setMsg(exception.getMessage());
        }};
    }
}
