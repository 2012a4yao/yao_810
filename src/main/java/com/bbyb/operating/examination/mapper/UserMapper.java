package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.po.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    int insert(User row);

    User selectByPrimaryKey(Integer id);

    List<User> selectAll();

    User selectUserCode(String userCode);

    User selectUserName(String userName);

    List<RoleUser> selectRoleUser(Integer userId);

    int delUser(Integer userId);

    int updateUser(User user);
}
