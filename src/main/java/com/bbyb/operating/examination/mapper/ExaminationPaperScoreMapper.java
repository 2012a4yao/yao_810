package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.ExaminationPaperScore;
import java.util.List;

public interface ExaminationPaperScoreMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExaminationPaperScore row);

    ExaminationPaperScore selectByPrimaryKey(Integer id);

    List<ExaminationPaperScore> selectAll();

    int updateByPrimaryKey(ExaminationPaperScore row);
}