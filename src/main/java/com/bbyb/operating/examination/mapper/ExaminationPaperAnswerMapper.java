package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.ExaminationPaperAnswer;
import java.util.List;

public interface ExaminationPaperAnswerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExaminationPaperAnswer row);

    ExaminationPaperAnswer selectByPrimaryKey(Integer id);

    List<ExaminationPaperAnswer> selectAll();

    int updateByPrimaryKey(ExaminationPaperAnswer row);
}