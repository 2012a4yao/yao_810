package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.RoleUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role row);

    Role selectByPrimaryKey(Integer id);

    List<Role> selectAll();

    int updateByPrimaryKey(Role row);

    Role selectRoleCode(String roleCode);

    Role selectRoleName(String roleName);

    List<RoleUser> selectRoleUser(Integer id);
}
