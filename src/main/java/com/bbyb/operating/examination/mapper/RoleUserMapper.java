package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.RoleUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleUserMapper {
//    int deleteByPrimaryKey(Integer id);
//
//    RoleUser selectByPrimaryKey(Integer id);
//
//    List<RoleUser> selectAll();
//
//    int updateByPrimaryKey(RoleUser row);

    int addRoleUser(RoleUser roleUser);
}
