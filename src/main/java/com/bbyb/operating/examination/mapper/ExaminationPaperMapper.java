package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.ExaminationPaper;
import java.util.List;

public interface ExaminationPaperMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExaminationPaper row);

    ExaminationPaper selectByPrimaryKey(Integer id);

    List<ExaminationPaper> selectAll();

    int updateByPrimaryKey(ExaminationPaper row);
}