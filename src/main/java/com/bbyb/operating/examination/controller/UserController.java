package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 账号
 * className: UserController
 * datetime: 2023/2/10 14:28
 * author: lx
 */
@RestController()
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 新增用户
     * @param user
     * @return
     */
    @PostMapping(value = "add")
    public CommonResult<Boolean> addUser(@RequestBody User user) {
        CommonResult<Boolean> result = userService.addUser(user);
        return result;
    }

    /**
     * 删除用户
     * @param userId
     * @return
     */
    @PostMapping(value = "del/{userId}")
    public CommonResult delUser(@PathVariable Integer userId) {
        CommonResult result = userService.delUser(userId);
        return result;
    }

    /**
     *查询所有的用户
     * @return
     */
    @PostMapping("/allUser")
    public List<User> getAllUser() {
        return userService.getAllUser();
    }

    /**
     * 修改用户
     * @param user
     * @return
     */
    @PostMapping("/updateUser")
    public CommonResult updateUser(@RequestBody User user) {
       CommonResult result = userService.updateUser(user);
       return result;
    }



}



