package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 姚某
 * data 2023/8/10 19:19
 * fileName RoleController
 */
@RestController
@RequestMapping("/role")
@Slf4j
public class RoleController {
    @Autowired
    private RoleService roleService;

    /**
     * 增加角色
     * @param role
     * @return
     */
    @PostMapping(value = "add")
    public CommonResult addRole(@RequestBody Role role) {
        CommonResult result = roleService.addRole(role);
        return result;
    }

    /**
     * 删除角色
     * @param id
     * @return
     */
    @PostMapping(value = "del/{id}")
    public CommonResult delUser(@PathVariable Integer id) {
        CommonResult result = roleService.delRole(id);
        return result;
    }

    /**
     * 修改角色
     * @param role
     * @return
     */
    @PostMapping("/updateRole")
    public CommonResult updateUser(@RequestBody Role role) {
        CommonResult result = roleService.updateRole(role);
        return result;
    }

    /**
     *查询所有的用户
     * @return
     */
    @PostMapping("/allRole")
    public List<Role> getAllUser() {
        return roleService.allRole();
    }

}
