package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 姚某
 * data 2023/8/10 18:54
 * fileName RoleUserController
 */
@RestController
@RequestMapping("/roleUser")
public class RoleUserController {

    @Autowired
    private RoleUserService roleUserService;

    /**
     * 新增角色
     * @param roleUser
     * @return
     */
    @PostMapping(value = "add")
    public CommonResult<Boolean> addRoleUser(@RequestBody RoleUser roleUser) {
        CommonResult<Boolean> result = roleUserService.addRoleUser(roleUser);
        return result;
    }


}
