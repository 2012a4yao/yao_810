package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.vo.CommonResult;

/**
 * @author 姚某
 * data 2023/8/10 18:55
 * fileName RoleUserService
 */
public interface RoleUserService {
    CommonResult<Boolean> addRoleUser(RoleUser user);
}
