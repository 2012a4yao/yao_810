package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;

import java.util.List;

/**
 * @author 姚某
 * data 2023/8/10 19:20
 * fileName RoleService
 */
public interface RoleService {
    CommonResult addRole(Role role);

    CommonResult delRole(Integer id);

    CommonResult updateRole(Role role);

    List<Role> allRole();
}
