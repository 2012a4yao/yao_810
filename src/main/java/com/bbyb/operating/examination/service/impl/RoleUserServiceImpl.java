package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.RoleUserMapper;
import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 姚某
 * data 2023/8/10 18:56
 * fileName RoleUserServiceImpl
 */
@Service
public class RoleUserServiceImpl implements RoleUserService {
    @Autowired
    private RoleUserMapper roleUserMapper;

    @Override
    public CommonResult<Boolean> addRoleUser(RoleUser roleUser) {
        if (roleUser == null) {
            return new CommonResult<>(601, "用户信息不能为空");
        }
        if (null==roleUser.getRoleId()) {
            return new CommonResult<>(601, "用户角色id不能为空");
        }
        if (null!=roleUser.getUserId()) {
            return new CommonResult<>(601,"用户id不能为空");
        }
        if (StringUtils.isBlank(roleUser.getCreateBy())) {
            return new CommonResult<>(601,"创建人不能为空");
        }
        if (roleUserMapper.addRoleUser(roleUser) != 1 ) {
            return new CommonResult<>(500, "保存角色信息失败");
        }
        return new CommonResult<>(200,"添加成功");
    }
}
