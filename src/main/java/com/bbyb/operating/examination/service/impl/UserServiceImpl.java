package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.UserMapper;
import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 账号服务
 * className: UserServiceImpl
 * datetime: 2023/2/10 14:29
 * author: lx
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    @Override
    public List<User> getAllUser() {
       List<User> list = userMapper.selectAll();
       list.stream().forEach(user -> {
           log.info(user.getUserName(),user.getUserCode());
       });
       return list;
    }

    @Override
    public CommonResult<Boolean> addUser(User user) {
        if (user == null) {
            return new CommonResult<>(601, "用户信息不能为空");
        }

        if (StringUtils.isBlank(user.getUserCode())) {
            return new CommonResult<>(601, "用户编码不能为空");
        }

        if (StringUtils.isBlank(user.getUserName())) {
            return new CommonResult<>(601, "用户名不能为空");
        }

        if (StringUtils.isBlank(user.getPhone())) {
            return new CommonResult<>(601, "用户手机号不能为空");
        }

        if (StringUtils.isBlank(user.getPassword())) {
            return new CommonResult<>(601, "用户密码不能为空");
        }
        User user1 = userMapper.selectUserCode(user.getUserCode());
        if (null != user1) {
            return new CommonResult<>(601, "角色编码已重复");
        }
        User user2 = userMapper.selectUserName(user.getUserName());
        if (null != user2) {
            return new CommonResult<>(601, "角色名称已重复");
        }

        if (userMapper.insert(user) != 1) {
            return new CommonResult<>(500, "保存用户信息失败");
        }
        return new CommonResult<>(200, "添加成功");

    }

    @Override
    public CommonResult delUser(Integer userId) {
        List<RoleUser> roleUsers = userMapper.selectRoleUser(userId);
        if (0 != roleUsers.size() ) {
            return new CommonResult(500, "存在账号关联的角色不允许删除");
        }
        if (userMapper.delUser(userId) != 1) {
            return new CommonResult<>(500, "删除用户失败");
        }
        return new CommonResult(200, "删除成功");
    }

    @Override
    public CommonResult updateUser(User user) {
        if (userMapper.updateUser(user) != 1) {
            return new CommonResult(500,"修改失败");
        }
        return new CommonResult(200,"修改成功");
    }


}
