package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;

import java.util.List;

public interface UserService {


    List<User> getAllUser();

    CommonResult<Boolean> addUser(User user);

    CommonResult delUser(Integer userId);

    CommonResult updateUser(User user);
}
