package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.RoleMapper;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 姚某
 * data 2023/8/10 19:20
 * fileName RoleServiceImpl
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public CommonResult addRole(Role role) {
        if (role == null) {
            return new CommonResult<>(601, "角色信息不能为空");
        }

        if (StringUtils.isBlank(role.getRoleCode())) {
            return new CommonResult<>(601, "角色编码不能为空");
        }
        if (StringUtils.isBlank(role.getRoleName())) {
            return new CommonResult<>(601, "角色名不能为空");
        }

        Role role1 = roleMapper.selectRoleCode(role.getRoleCode());
        if (null != role1) {
            return new CommonResult(500, "角色编码已重复");
        }
        Role role2 = roleMapper.selectRoleName(role.getRoleName());
        if (null != role2) {
            return new CommonResult(500, "角色名称已重复");
        }
        if (roleMapper.insert(role) != 1) {
            return new CommonResult(500, "添加角色失败");
        }
        return new CommonResult(200, "添加成功");
    }

    @Override
    public CommonResult delRole(Integer id) {
        List<RoleUser> roleUsers = roleMapper.selectRoleUser(id);
        if (0 != roleUsers.size()) {
            return new CommonResult(500, "存在账号关联的角色不允许删除");
        }
        if (roleMapper.deleteByPrimaryKey(id) != 1) {
            return new CommonResult<>(500, "删除角色失败");
        }
        return new CommonResult(200, "删除成功");

    }

    @Override
    public CommonResult updateRole(Role role) {
        if (roleMapper.updateByPrimaryKey(role) != 1) {
            return new CommonResult(500,"修改失败");
        }
        return new CommonResult(200,"修改成功");
    }

    @Override
    public List<Role> allRole() {
        List<Role> list = roleMapper.selectAll();
        return list;
    }
}
